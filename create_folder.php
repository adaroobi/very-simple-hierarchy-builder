<?php

if (strtolower($_SERVER['REQUEST_METHOD']) !== 'post') header('Location:folder_form.php');

$parentID = !empty($_GET['parentID']) ? $_GET['parentID'] : 0;;
$folderName = empty($_POST['folderName']) ? "New Folder" : $_POST['folderName'];

try {
    $database = new PDO("mysql:host=localhost;dbname=scotchbox;charset=utf8mb4", "root", "root", [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_PERSISTENT => false
    ]);
} catch (PDOException $e) {
    die("Error Connecting to Database..");
}

$statement = $database->prepare("INSERT INTO `tree` (`node_parent`, `node_name`) VALUES (?, ?)");

if ($statement->execute([$parentID, $folderName])) {
    header("Location:folder_form.php?parentID={$database->lastInsertId()}");
} else {
    die("Error Inserting folder [{$folderName}]. Please try again");
}