<?php
try {
    $database = new PDO("mysql:host=localhost;dbname=scotchbox;charset=utf8mb4", "root", "root", [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_PERSISTENT => false
    ]);
} catch (PDOException $e) {
    die("Error Connecting to Database..");
}

$parentID = !empty($_GET['parentID']) ? $_GET['parentID'] : 0;

if ($parentID) {
    $currentFolder = $database->prepare("SELECT * FROM `tree` WHERE `node_id` = ?");

    if (!$currentFolder->execute([$parentID])) die("Wrong Parent ID");

    $currentFolderArray = $currentFolder->fetchAll(PDO::FETCH_ASSOC);

    $currentFolderName = $currentFolderArray[0]['node_name'];
}

$currentFolderName = empty($currentFolderName)? 'root': $currentFolderName;

function buildTreeHTML(&$list, $parentID = 0) {

    $isBranch = false;

    foreach ($list as $index => $element) {

        if ($list[$index]['node_parent'] != $parentID) continue;

        if (!$isBranch) {
            echo '<ul>';
            $isBranch = true;
        }

        echo "<li><a href='folder_form.php?parentID={$list[$index]['node_id']}'>" . $list[$index]['node_name'] . '</a></li>';

        buildTreeHTML($list, $list[$index]['node_id']);
    }

    if ($isBranch) {
        echo '</ul>';
    }
}

?>
<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8">
    <title>نموذج الإدخال</title>
</head>
<body>
<h2>Current Folder: <?= $currentFolderName ?></h2>
<form method="post" action="create_folder.php?parentID=<?= $parentID ?>">
    <label for="folderName">Create Folder</label>
    <input id="folderName" name="folderName" type="text" placeholder="Create Folder">
    <input type="submit">
</form>

<hr>

<h2>Folders Tree</h2>
<ul>
    <li><a href="folder_form.php">root</a></li>
    <?php
    $treeObject = $database->query("SELECT * FROM `tree`");
    $treeArray = $treeObject->fetchAll(PDO::FETCH_ASSOC);

    buildTreeHTML($treeArray);
    ?>
</ul>

</body>
</html>